package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.Servo;
@Autonomous(name="Servo Test", group="Linear Opmode")
@Disabled
public class ServoTest extends LinearOpMode {
    public Servo gheara = null;

    @Override
    public void runOpMode() {
        gheara = hardwareMap.get(Servo.class, "servo");

        waitForStart();

        while(opModeIsActive()) {
            if (gamepad1.a) {
                gheara.setPosition(0.5);
                sleep(100);
            } else if (gamepad1.x) {
                gheara.setPosition(0);
                sleep(100);
            }
        }
    }
}
