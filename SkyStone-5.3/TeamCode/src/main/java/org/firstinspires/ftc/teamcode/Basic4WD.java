package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;

@TeleOp(name="4 Wheel Drive", group="Linear Opmode")
@Disabled
public class Basic4WD extends LinearOpMode {

    public DcMotor leftFront = null;        public DcMotor rightFront = null;
    public DcMotor leftBack = null;         public DcMotor rightBack = null;

    double x1, x2, y1, y2, mult1, mult2;

    @Override
    public void runOpMode() {
        telemetry.addData("Status", "Initialized");
        telemetry.update();

        leftFront  = hardwareMap.get(DcMotor.class, "left_front");
        rightFront = hardwareMap.get(DcMotor.class, "right_front");
        leftBack  = hardwareMap.get(DcMotor.class, "left_back");
        rightBack = hardwareMap.get(DcMotor.class, "right_back");

        leftFront.setDirection(DcMotor.Direction.FORWARD);
        leftBack.setDirection(DcMotor.Direction.FORWARD);
        rightFront.setDirection(DcMotor.Direction.REVERSE);
        rightBack.setDirection(DcMotor.Direction.REVERSE);

        waitForStart();

        while (opModeIsActive()) {

            x1 = gamepad1.left_stick_x;         y1 = -gamepad1.left_stick_y;
            x2 = gamepad1.right_stick_x;        y2 = -gamepad2.right_stick_y;

            mult1 = 1;
            mult2 = 1;

            if (gamepad1.left_bumper)
                mult1 = 0.5;
            else if (gamepad1.left_trigger != 0)
                mult1 = 0.25;

            if (gamepad2.right_bumper)
                mult2 = 0.5;
            else if (gamepad1.left_trigger != 0)
                mult2 = 0.25;

            if (x1 == 0 && y1 == 0) {
                leftFront.setPower(0);      rightFront.setPower(0);
                leftBack.setPower(0);       rightBack.setPower(0);
            }

            else if (x1 >= 0 && y1 >= 0) {
                if (x1 >= y1) {
                    leftFront.setPower(mult1);                      rightFront.setPower(mult1 * (y1 / x1 - 1));
                    leftBack.setPower(mult1 * (y1 / x1 - 1));       rightBack.setPower(mult1);
                }
                else {
                    leftFront.setPower(mult1);                      rightFront.setPower(mult1 * (x1 / y1 - 1));
                    leftBack.setPower(mult1 * (x1 / y1 - 1));       rightBack.setPower(mult1);
                }
            }

            else if (x1 <= 0 && y1 >= 0) {
                if (-x1 <= y1) {
                    leftFront.setPower(mult1 * (x1 / y1 + 1));      rightFront.setPower(mult1);
                    leftBack.setPower(mult1);                       rightBack.setPower(mult1 * (x1 / y1 + 1));
                }
                else {
                    leftFront.setPower(mult1 * (-y1 / x1 - 1));     rightFront.setPower(mult1);
                    leftBack.setPower(mult1);                       rightBack.setPower(mult1 * (-y1 / x1 - 1));
                }
            }

            else if (x1 <= 0 && y1 <= 0) {
                if (x1 <= y1) {
                    leftFront.setPower(-mult1);                     rightFront.setPower(mult1 * (y1 / x1 - 1));
                    leftBack.setPower(mult1 * (y1 / x1 - 1));       rightBack.setPower(-mult1);
                }
                else {
                    leftFront.setPower(-mult1);                     rightFront.setPower(mult1 * (-x1 / y1 + 1));
                    leftBack.setPower(mult1 * (-x1 / y1 + 1));      rightBack.setPower(-mult1);
                }
            }

            else if (x1 >= -y1) {
                leftFront.setPower(mult1 * (y1 / x1 + 1));          rightFront.setPower(-mult1);
                leftBack.setPower(-mult1);                          rightBack.setPower(mult1 * (y1 / x1 + 1));
            }
            else {
                leftFront.setPower(mult1 * (-x1 / y1 - 1));         rightFront.setPower(-mult1);
                leftBack.setPower(-mult1);                          rightBack.setPower(mult1 * (-x1 / y1 - 1));
            }


            if (Math.abs(x2) >= Math.abs(y2)) {
                leftFront.setPower(mult2);      rightFront.setPower(-mult2);
                leftBack.setPower(mult2);       rightBack.setPower(-mult2);
            }
            // else actionare brat

            telemetry.addData("Left stick:", "X (%.2f), Y (%.2f)", x1, y1);
            telemetry.addData("Right stick:", "X (%.2f), Y (%.2f)", x2, y2);
            telemetry.addData("Front motors:", "left (%.2f), right (%.2f)", leftFront.getPower(), rightFront.getPower());
            telemetry.addData("Back motors:", "left (%.2f), right (%.2f)", leftBack.getPower(), rightBack.getPower());
            telemetry.addData("Multipliers:", "left (%.2f), right (%.2f)", mult1, mult2);
            telemetry.update();
        }
    }
}