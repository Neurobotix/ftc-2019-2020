package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import static org.firstinspires.ftc.teamcode.Functions.biasfata;
import static org.firstinspires.ftc.teamcode.Functions.biasspate;

@TeleOp(name="2 Wheel Drive", group="Linear Opmode")

public class Basic2WD extends LinearOpMode {

    public DcMotor left = null, right = null;

    double x, y, mult;

    @Override
    public void runOpMode() {
        telemetry.addData("Status", "Initialized");
        telemetry.update();

        left = hardwareMap.get(DcMotor.class, "left");
        right = hardwareMap.get(DcMotor.class, "right");
        left.setDirection(DcMotor.Direction.FORWARD);
        right.setDirection(DcMotor.Direction.REVERSE);

        waitForStart();

        while (opModeIsActive()) {

            x = gamepad1.left_stick_x;
            y = -gamepad1.left_stick_y;

            mult = 1;

            if(gamepad1.left_bumper)
                mult = 0.5;
            else if(gamepad1.left_trigger != 0)
                mult = 0.25;

            if(x == 0 && y == 0) {
                left.setPower(0);
                right.setPower(0);
            }
            else if(Math.abs(x) > Math.abs(y)) {
                if(x > 0) {
                    left.setPower(-mult);
                    right.setPower(mult*biasfata);
                }
                else {
                    left.setPower(mult);
                    right.setPower(-mult*biasspate);
                }
            }
            else if(y > 0) {
                left.setPower(mult);
                right.setPower(mult*biasfata);
            }
            else {
                left.setPower(-mult);
                right.setPower(-mult*biasspate);
            }
        }
    }
}