package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;

@Autonomous(name="Autonomie", group="Linear Opmode")
@Disabled

public class Autonomy extends LinearOpMode {
    public static final int drivespeed = 1;
    public static final int turnspeed = 1;

    public void Move(int distance) {
        int time = 1000 * distance / drivespeed;
        if(distance > 0) {
            left.setPower(1);
            right.setPower(1);
        }
        else {
            left.setPower(-1);
            right.setPower(-1);
        }
        sleep(time);
        left.setPower(0);
        right.setPower(0);
    }

    public void Turn(int angle) {
        int time = 1000 * angle / turnspeed;
        if(angle < 0) {
            left.setPower(-1);
            right.setPower(1);
        }
        else {
            left.setPower(1);
            right.setPower(-1);
        }
        sleep(time);

        left.setPower(0);
        right.setPower(0);
    }

    public DcMotor left = null, right = null;

    @Override
    public void runOpMode() {
        telemetry.addData("Status", "Initialized");
        telemetry.update();

        left  = hardwareMap.get(DcMotor.class, "left");
        right = hardwareMap.get(DcMotor.class, "right");

        left.setDirection(DcMotor.Direction.FORWARD);
        right.setDirection(DcMotor.Direction.REVERSE);

        waitForStart();

        Move(122);
        Turn(180);
        Move(61);
        Turn(90);
        Move(162);
        Move(-81);
    }
}
