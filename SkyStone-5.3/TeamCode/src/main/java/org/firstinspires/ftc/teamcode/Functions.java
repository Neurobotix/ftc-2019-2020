package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.DcMotor;

public abstract class Functions {
    public static final void sleep(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public static final int drivespeed = 75, turnspeed = 150;
    public static final double biasfata = 0.79, biasspate = 0.65;

    public static void Move(int distance, DcMotor left, DcMotor right) {
        int time = 1000 * Math.abs(distance) / drivespeed;
        if(distance > 0) {
            left.setPower(1);
            right.setPower(biasfata);
        }
        else {
            left.setPower(-1);
            right.setPower(-biasspate);
        }
        sleep(time);

        left.setPower(0);
        right.setPower(0);
    }

    public static void Turn(int angle, DcMotor left, DcMotor right) {
        int time = 1000 * Math.abs(angle) / turnspeed;
        if(angle < 0) {
            left.setPower(-1);
            right.setPower(biasfata);
        }
        else {
            left.setPower(1);
            right.setPower(-biasspate);
        }
        sleep(time);

        left.setPower(0);
        right.setPower(0);
    }
}
