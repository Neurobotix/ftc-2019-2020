package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;

@Autonomous(name="Parcare Red Loading", group="Linear Opmode")

public class ParcareRedBuilding extends LinearOpMode {

    public DcMotor left = null, right = null;

    void Move(int distance) {Functions.Move(distance, left, right);}
    void Turn(int angle) {Functions.Turn(angle, left, right);}

    @Override
    public void runOpMode() {
        telemetry.addData("Status", "Initialized");
        telemetry.update();

        left  = hardwareMap.get(DcMotor.class, "left");
        right = hardwareMap.get(DcMotor.class, "right");

        left.setDirection(DcMotor.Direction.FORWARD);
        right.setDirection(DcMotor.Direction.REVERSE);

        waitForStart();

        Move(50);
        Turn(-90);
        Move(50);
    }
}
