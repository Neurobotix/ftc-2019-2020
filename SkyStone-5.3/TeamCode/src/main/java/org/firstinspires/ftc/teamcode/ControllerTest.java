package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;

@Autonomous(name="Value test", group="Linear Opmode")

public class ControllerTest extends LinearOpMode {
    public DcMotor left = null, right = null;
    int distance = 100, degrees = 90;

    int drivespeed = 75, turnspeed = 90;
    double biasfata = 0.79, biasspate = 0.65;

    public void Move(int distance) {
        int time = 1000 * Math.abs(distance) / drivespeed;
        if(distance > 0) {
            left.setPower(1);
            right.setPower(biasfata);
        }
        else {
            left.setPower(-1);
            right.setPower(-biasspate);
        }
        sleep(time);

        left.setPower(0);
        right.setPower(0);
    }

    public void Turn(int angle) {
        int time = 1000 * Math.abs(angle) / turnspeed;
        if(angle < 0) {
            left.setPower(-1);
            right.setPower(biasfata);
        }
        else {
            left.setPower(1);
            right.setPower(-biasspate);
        }
        sleep(time);

        left.setPower(0);
        right.setPower(0);
    }

    @Override
    public void runOpMode() {
        telemetry.addData("Status", "Initialized");
        telemetry.update();
        left = hardwareMap.get(DcMotor.class, "left");
        right = hardwareMap.get(DcMotor.class, "right");
        left.setDirection(DcMotor.Direction.FORWARD);
        right.setDirection(DcMotor.Direction.REVERSE);

        waitForStart();

        while(opModeIsActive()) {



            if(gamepad1.y) {
                Move(distance);
                sleep(50);
            }
            else if(gamepad1.a) {
                Move(-distance);
                sleep(50);
            }
            else if(gamepad1.b) {
                Turn(-degrees);
                sleep(50);
            }
            else if(gamepad1.x) {
                Turn(degrees);
                sleep(50);
            }
            else if(gamepad1.dpad_up) {
                distance += 5;
                sleep(50);
            }
            else if(gamepad1.dpad_down) {
                distance -= 5;
                sleep(50);
            }
            else if(gamepad1.dpad_right) {
                degrees += 5;
                sleep(50);
            }
            else if(gamepad1.dpad_left) {
                degrees -= 5;
                sleep(50);
            }
            else if(gamepad1.left_bumper) {
                turnspeed -= 5;
                sleep(50);
            }
            else if(gamepad1.right_bumper) {
                turnspeed += 5;
                sleep(50);
            }
            else if(gamepad1.left_trigger != 0) {
                drivespeed -= 5;
                sleep(50);
            }
            else if(gamepad1.right_trigger != 0) {
                drivespeed += 5;
                sleep(50);
            }

            telemetry.addData("Drivespeed", drivespeed);
            telemetry.addData("Turnspeed", turnspeed);
            telemetry.addData("Distance", distance);
            telemetry.addData("Degrees", degrees);
            telemetry.update();
        }
    }
}