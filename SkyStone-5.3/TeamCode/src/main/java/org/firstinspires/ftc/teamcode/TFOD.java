package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import java.util.List;
import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer.CameraDirection;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;

@Autonomous (name = "TensorFlow Object Detection", group = "Concept")
@Disabled
public class
TFOD extends LinearOpMode {
    private static final String TFOD_MODEL_ASSET = "Skystone.tflite";
    private static final String LABEL_FIRST_ELEMENT = "Stone";
    private static final String LABEL_SECOND_ELEMENT = "Skystone";

    private static final String VUFORIA_KEY =
            "AZuv5uj/////AAABmWki6SKu509+p+UUYGmtf3RlQXndFjBsKPG7zybrrhYYCH0JsQnFQeJUWtW4GkQf1oirG2L41VbjY3BZ940WsGTAfNZ64dVPcekcfhDwIymQz+SE8kxmn/7MWhlVdtyrGiwdD+irlj6WvweKmo4sf097PYRYbg2Ow73VzVGDRin6M6s2PnTDt3zwvEKEWlSHyipN8i00iaoYwofH00s0qdlLF7/be6kMda+dxZ455C0t6hEeuwpRUE7kH8fy9+VgKj+7WbXJfsyPta4vlVdbf8zva+pNlSVvvrse3Y0SQ7tt960YysszkUVeipLEj4XIeUV/EjgEkmPrU9qGvkvC05MXODtO59XrDNq/y7qyENT7";

    private VuforiaLocalizer vuforia;

    private TFObjectDetector tfod;

    public Recognition max1;

    @Override
    public void runOpMode() {
        initVuforia();

        if (ClassFactory.getInstance().canCreateTFObjectDetector()) {
            initTfod();
        }
        else {
            telemetry.addData("Sorry!", "This device is not compatible with TFOD");
        }

        if (tfod != null) {
            tfod.activate();
        }

        telemetry.addData(">", "Press Play to start op mode");
        telemetry.update();
        waitForStart();

        if (opModeIsActive()) {
            while (opModeIsActive()) {
                if (tfod != null) {
                    List<Recognition> updatedRecognitions = tfod.getUpdatedRecognitions();
                    if (updatedRecognitions != null) {
                        telemetry.addData("# Object Detected", updatedRecognitions.size());

                        int i = 0;
                        for (int j=0;j<updatedRecognitions.size()-1;j++) {

                            if(updatedRecognitions.get(j).getLeft() > updatedRecognitions.get(j+1).getLeft())
                                max1 = updatedRecognitions.get(j);
                            else if(updatedRecognitions.get(j).getLeft() < updatedRecognitions.get(j+1).getLeft())
                                max1 = updatedRecognitions.get(j+1);

                            telemetry.addData("Piece location (%.2d)", updatedRecognitions.indexOf(max1));

                        }
                        telemetry.update();
                    }
                }
            }
        }

        if (tfod != null) {
            tfod.shutdown();
        }
    }

    /**
     * Initialize the Vuforia localization engine.
     */
    private void initVuforia() {
        /*
         * Configure Vuforia by creating a Parameter object, and passing it to the Vuforia engine.
         */
        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters();

        parameters.vuforiaLicenseKey = VUFORIA_KEY;
        parameters.cameraDirection = CameraDirection.BACK;

        //  Instantiate the Vuforia engine
        vuforia = ClassFactory.getInstance().createVuforia(parameters);

        // Loading trackables is not necessary for the TensorFlow Object Detection engine.
    }

    /**
     * Initialize the TensorFlow Object Detection engine.
     */
    private void initTfod() {
        int tfodMonitorViewId = hardwareMap.appContext.getResources().getIdentifier(
                "tfodMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        TFObjectDetector.Parameters tfodParameters = new TFObjectDetector.Parameters(tfodMonitorViewId);
        tfodParameters.minimumConfidence = 0.8;
        tfod = ClassFactory.getInstance().createTFObjectDetector(tfodParameters, vuforia);
        tfod.loadModelFromAsset(TFOD_MODEL_ASSET, LABEL_FIRST_ELEMENT, LABEL_SECOND_ELEMENT);
    }
}